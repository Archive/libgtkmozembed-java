/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed.event;

/**
 * An abstract adapter class for receiving MozEmbed net events.
 */
public abstract class NetAdapter implements NetListener {

    /**
     * Invoked when the progress of a document download has changed.
     *
     * @see NetEvent.Type#PROGRESS
     */
    public void progressUpdate( NetEvent event ) {}
    /**
     * Invoked when the progress of a document download has changed.
     *
     * @see NetEvent.Type#PROGRESS_ALL
     */
    public void progressAllUpdate( NetEvent event ) {}
    /**
     * Invoked when the network connection state has changed.
     *
     * @see NetEvent.Type#NET_STATE
     */
    public void netStateChange( NetEvent event ) {}
    /**
     * Invoked when the network connection state has changed.
     *
     * @see NetEvent.Type#NET_STATE_ALL
     */
    public void netStateAllChange( NetEvent event ) {}
    /**
     * Invoked at the start of the network activities required for
     * downloading a document.
     *
     * @see NetEvent.Type#NET_START
     */
    public void netStart( NetEvent event ) {}
    /**
     * Invoked at the end of the network activities required for
     * downloading a document.
     *
     * @see NetEvent.Type#NET_STOP
     */
    public void netStop( NetEvent event ) {}
}
