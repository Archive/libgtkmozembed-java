/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed.event;

import org.gnu.glib.EventType;
import org.gnu.gtkmozembed.MozEmbed;

/**
 * Class for MozEmbed network events.
 */
public class NetEvent extends MozEmbedEvent {

    /**
     * Types of NetEvents.
     */
    public static class Type extends EventType {

        private Type( int id, String name ) {
            super( id, name );
        }

        /**
         * Signals that the progress of the document download has changed.
         * <p>
         * Data available: 
         * <ul>
         * <li>{@link #getCurrent}</li>
         * <li>{@link #getMaximum}</li>
         * </ul>
         */
        public static final Type PROGRESS = 
            new Type( 1, "PROGRESS" );
        /**
         * Signals that the progress of the document download has changed.
         * <p>
         * Data available: 
         * <ul>
         * <li>{@link #getCurrent}</li>
         * <li>{@link #getMaximum}</li>
         * <li>{@link #getURI}</li>
         * </ul>
         */
        public static final Type PROGRESS_ALL = 
            new Type( 2, "PROGRESS_ALL" );
        /**
         * Signals that the network connection state has changed.
         * <p>
         * Data available: 
         * <ul>
         * <li>{@link #getState}</li>
         * <li>{@link #getStatus}</li>
         * </ul>
         */
        public static final Type NET_STATE = 
            new Type( 3, "NET_STATE" );
        /**
         * Signals that the network connection state has changed.
         * <p>
         * Data available: 
         * <ul>
         * <li>{@link #getState}</li>
         * <li>{@link #getStatus}</li>
         * <li>{@link #getURI}</li>
         * </ul>
         */
        public static final Type NET_STATE_ALL = 
            new Type( 4, "NET_STATE_ALL" );
        /**
         * Signals the start of the network activities required for
         * downloading a document.
         * <p>
         * Data available: 
         * <ul>
         * <li>none</li>
         * </ul>
         */
        public static final Type NET_START = 
            new Type( 5, "NET_START" );
        /**
         * Signals the end of the network activities for a document.
         * <p>
         * Data available: 
         * <ul>
         * <li>none</li>
         * </ul>
         */
        public static final Type NET_STOP = 
            new Type( 6, "NET_STOP" );

    }

    /**
     * The current/state value.
     */
    private int cur;
    /**
     * The maximum/status value.
     */
    private int max;
    /**
     * the URI value.
     */
    private String uri;

    /** Get the current progress value. */
    public int getCurrent() { return cur; }
    /** Set the current progress value. */
    public void setCurrent( int val ) { cur = val; }
    /** Get the state value. */
    public int getState() { return cur; }
    /** Set the state value. */
    public void setState( int val ) { cur = val; }
    /** Get the maximum progress value. */
    public int getMaximum() { return max; }
    /** Set the maximum progress value. */
    public void setMaximum( int val ) { max = val; }
    /** Get the status value. */
    public int getStatus() { return max; }
    /** Set the status value. */
    public void setStatus( int val ) { max = val; }
    /** Get the URI value. */
    public String getURI() { return uri; }
    /** Set the URI value. */
    public void setURI( String val ) { uri = val; }

    /**
     * Creates a new MozEmbed net event. This is used internally
     * by java-gnome. Users only have to deal with listeners.
     */
    public NetEvent( MozEmbed source, NetEvent.Type type ) {
        super( source, type );
    }

    /**
     * Check if the given <tt>aType</tt> parameter is the same type as this
     * instance.
     * 
     * @return True if the type of this event is the same as that of the 
     * parameter.
     */
    public boolean isOfType( NetEvent.Type aType ) {
        return ( type.getID() == aType.getID() );
    }
}
