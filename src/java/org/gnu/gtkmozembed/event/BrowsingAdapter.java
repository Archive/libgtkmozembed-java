/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed.event;

/**
 * An abstract adapter class for receiving MozEmbed browsing
 * information events.
 */
public abstract class BrowsingAdapter implements BrowsingListener {

    /**
     * Invoked when the cursor has moved over/off a link.
     *
     * @see BrowsingEvent.Type#LINK_MESSAGE
     */
    public void linkMessageChanged( BrowsingEvent event ) {}
    /**
     * Invoked when the JavaScript message has changed.
     *
     * @see BrowsingEvent.Type#JS_STATUS
     */
    public void jsStatusChanged( BrowsingEvent event ) {}
    /**
     * Invoked when the location (URL) of a document is known.
     *
     * @see BrowsingEvent.Type#LOCATION
     */
    public void locationChanged( BrowsingEvent event ) {}
    /**
     * Invoked when the title of a document is known.
     *
     * @see BrowsingEvent.Type#TITLE
     */
    public void titleChanged( BrowsingEvent event ) {}

}
