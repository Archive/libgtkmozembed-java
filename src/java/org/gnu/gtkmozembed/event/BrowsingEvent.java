/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed.event;

import org.gnu.glib.EventType;
import org.gnu.gtkmozembed.MozEmbed;

/**
 * Class for MozEmbed browsing information events.
 */
public class BrowsingEvent extends MozEmbedEvent {

    /**
     * Types of BrowserEvents.
     */
    public static class Type extends EventType {

        private Type( int id, String name ) {
            super( id, name );
        }

        /**
         * Signals that the cursor has moved over/off a link.  If
         * the cursor has moved over the link, calling {@link
         * org.gnu.gtkmozembed.MozEmbed#getLinkMessage} will return
         * the link's message (the URL).  If the cursor has moved off
         * the link, calling {@link
         * org.gnu.gtkmozembed.MozEmbed#getLinkMessage} will return
         * <tt>null</tt>.
         */
        public static final Type LINK_MESSAGE = new Type( 1, "LINK_MESSAGE" );
        /**
         * Signals that the JavaScript status has changed.  The
         * current status can be obtained by calling {@link
         * org.gnu.gtkmozembed.MozEmbed#getJsStatus}.
         */
        public static final Type JS_STATUS = new Type( 2, "JS_STATUS" );
        /**
         * Signals that a document at location is being loaded.  The
         * current location can be obtained by calling {@link
         * org.gnu.gtkmozembed.MozEmbed#getLocation}.
         */
        public static final Type LOCATION = new Type( 3, "LOCATION" );
        /**
         * Signals that the documents title has been set.  The current
         * location can be obtained by calling {@link
         * org.gnu.gtkmozembed.MozEmbed#getTitle}.
         */
        public static final Type TITLE = new Type( 4, "TITLE" );
    }

    /**
     * Creates a new MozEmbed browsing event. This is used internally by 
     * java-gnome. Users only have to deal with listeners.
     */
    public BrowsingEvent( MozEmbed source, BrowsingEvent.Type type ) {
        super( source, type );
    }

    /**
     * Check if the given <tt>aType</tt> parameter is the same type as this
     * instance.
     * 
     * @return True if the type of this event is the same as that of the 
     * parameter.
     */
    public boolean isOfType( BrowsingEvent.Type aType ) {
        return ( type.getID() == aType.getID() );
    }
}
