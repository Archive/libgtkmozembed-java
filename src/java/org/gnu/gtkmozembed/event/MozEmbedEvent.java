/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed.event;

import org.gnu.glib.EventType;
import org.gnu.gtk.event.GtkEvent;

import org.gnu.gtkmozembed.MozEmbed;

/**
 * Base class for all MozEmbed events.
 */
public class MozEmbedEvent extends GtkEvent {

    /**
     * Creates a new MozEmbed event. This is used internally by 
     * java-gnome. Users only have to deal with listeners.
     */
    public MozEmbedEvent( MozEmbed source, EventType type ) {
        super( source, type );
    }

    /**
     * Get the source {@link org.gnu.gtkmozembed.MozEmbed MozEmbed} object
     * for this event.
     */
    public MozEmbed getMozEmbedSource() {
        return (MozEmbed)getSource();
    }

}
