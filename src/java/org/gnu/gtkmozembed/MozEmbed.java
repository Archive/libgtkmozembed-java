/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Vector;

import org.gnu.glib.EventMap;
import org.gnu.glib.Handle;
import org.gnu.glib.Type;
import org.gnu.gtk.Bin;
import org.gnu.gtkmozembed.event.BrowsingEvent;
import org.gnu.gtkmozembed.event.BrowsingListener;
import org.gnu.gtkmozembed.event.NetEvent;
import org.gnu.gtkmozembed.event.NetListener;

/**
 * The Mozilla web browsing capabilities as an embedable GTK widget.
 */
public class MozEmbed extends Bin {

    /**
     * Listeners for handling MozEmbed browsing events.
     */
    private Vector browsingListeners = null;
    /**
     * Listeners for handling MozEmbed net events.
     */
    private Vector netListeners = null;

    /**
     * Map to collect events.
     */
    private static EventMap evtMap = new EventMap();

    /**
     * Retrieve the runtime type used by the GLib library.
     */
    public static Type getType() {
        return new Type( gtk_moz_embed_get_type() );
    }

    /**
     * Public constructor.  Creates a new MozEmbed instance.
     */
    public MozEmbed() {
        super( gtk_moz_embed_new() );
    }

    public static void pushStartup() {
        gtk_moz_embed_push_startup();
    }

    public static void popStartup() {
        gtk_moz_embed_pop_startup();
    }

    public static void setCompPath( String path ) {
        gtk_moz_embed_set_comp_path( path );
    }

    public static void setProfilePath( String dir, String path ) {
        gtk_moz_embed_set_profile_path( dir, path );
    }

    /**
     * Load and display the given URL in the browser.  External
     * documents (images, css, etc) are also retrieved and displayed.
     */ 
    public void loadURL( String url ) {
        gtk_moz_embed_load_url( getHandle(), url );
    }

    /**
     * Stop any current document loading.
     */
    public void stopLoad() {
        gtk_moz_embed_stop_load( getHandle() );
    }

    /**
     * Reload the current document.
     */
    public void reload( ReloadFlags flag ) {
        gtk_moz_embed_reload( getHandle(), flag.getValue() );
    }

    /**
     * Verify that there is a document 1 back of the current document
     * in this object's document list.
     */
    public boolean canGoBack() {
        return gtk_moz_embed_can_go_back( getHandle() );
    }

    /**
     * Verify that there is a document 1 forward of the current
     * document in this object's document list.
     */
    public boolean canGoForward() {
        return gtk_moz_embed_can_go_forward( getHandle() );
    }

    /**
     * Load the document that is 1 back of the current document in
     * this object's document list.
     */
    public void goBack() {
        gtk_moz_embed_go_back( getHandle() );
    }

    /**
     * Load the document that is 1 forward of the current document in
     * this object's document list.
     */
    public void goForward() {
        gtk_moz_embed_go_forward( getHandle() );
    }

    /**
     * Render the document contained in the <tt>data</tt> parameter.
     * Any relative links to external elements in the document (such
     * as images, css files, etc) are loaded from the given
     * <tt>baseUri</tt>.
     *
     * @param data A String containing the document to render.
     * @param baseUri The location from which to load any relative external
     * files.
     * @param mimeType The mime-type of the document (text/html for example).
     */
    public void renderData( String data, String baseUri, String mimeType ) {
        gtk_moz_embed_render_data( getHandle(), data, data.length(), 
                                   baseUri, mimeType );
    }

    /**
     * Render the document contained in the <tt>stream</tt> parameter.
     * Any relative links to external elements in the document (such
     * as images, css files, etc) are loaded from the given
     * <tt>baseUri</tt>.
     *
     * @param stream An InputStream containing the document to render.
     * The stream will be wrapped internally so that reading from it
     * is done efficiently.
     * @param baseUri The location from which to load any relative
     * external files.
     * @param mimeType The mime-type of the document (text/html for example).
     * @throws IOException If there was an error reading from the
     * <tt>stream</tt>.
     */
    public void renderData( InputStream stream,
                            String baseUri, String mimeType ) 
        throws IOException {

        InputStreamReader ir = new InputStreamReader( stream );
        renderData( ir, baseUri, mimeType );
    }

    /**
     * Render the document contained in the <tt>reader</tt> parameter.
     * Any relative links to external elements in the document (such
     * as images, css files, etc) are loaded from the given
     * <tt>baseUri</tt>.
     *
     * @param reader A Reader containing the document to render.  The
     * reader will be wrapped internally so that reading from it is
     * done efficiently.
     * @param baseUri The location from which to load any relative
     * external files.
     * @param mimeType The mime-type of the document (text/html for example).
     * @throws IOException If there was an error reading from the
     * <tt>reader</tt>.
     */
    public void renderData( Reader reader,
                            String baseUri, String mimeType ) 
        throws IOException {

        BufferedReader bread = new BufferedReader( reader );

        char raw[] = new char[4096];
        int len = -1;

        openStream( baseUri, mimeType );

        try {
            while ( ( len = bread.read( raw ) ) != -1 ) {
                appendData( String.valueOf( raw, 0, len ) );
            }
        } finally {
            closeStream();
        }
    }

    /**
     * Open a document stream for "manual" document loading.  Any
     * relative links to external elements in the document (such as
     * images, css files, etc) will be loaded from the given
     * <tt>baseUri</tt>.
     *
     * @param baseUri The location from which to load any relative
     * external files.
     * @param mimeType The mime-type of the document (text/html for example).
     * @see #closeStream
     */
    public void openStream( String baseUri, String mimeType ) {
        gtk_moz_embed_open_stream( getHandle(), baseUri, mimeType );
    }

    /**
     * Append the portion of a document contained in the <tt>data</tt>
     * parameter to the currently open document stream.  The document
     * stream must be opened using {@link #openStream}
     * <strong>before</strong> calling this method.  Any relative
     * links to external elements in the document (such as images, css
     * files, etc) are loaded from the base URI defined when calling 
     * {@link #openStream}.
     *
     * @param data A String containing the portion of a document to render.
     * @see #openStream
     */
    public void appendData( String data ) {
        gtk_moz_embed_append_data( getHandle(), data, data.length() );
    }

    /**
     * Append the portion of a document contained in the
     * <tt>stream</tt> parameter to the currently open document
     * stream.  The document stream must be opened using {@link
     * #openStream} <strong>before</strong> calling this method.  Any
     * relative links to external elements in the document (such as
     * images, css files, etc) are loaded from the base URI defined
     * when calling {@link #openStream}.
     *
     * @param stream An InputStream containing the portion of a
     * document to render.
     * @throws IOException If there was an error reading from the
     * <tt>stream</tt>.
     * @see #openStream
     */
    public void appendData( InputStream stream ) 
        throws IOException {

        InputStreamReader ir = new InputStreamReader( stream );
        appendData( ir );
    }

    /**
     * Append the portion of a document contained in the
     * <tt>reader</tt> parameter to the currently open document
     * stream.  The document stream must be opened using {@link
     * #openStream} <strong>before</strong> calling this method.  Any
     * relative links to external elements in the document (such as
     * images, css files, etc) are loaded from the base URI defined
     * when calling {@link #openStream}.
     *
     * @param reader A Reader containing the portion of a
     * document to render.
     * @throws IOException If there was an error reading from the
     * <tt>reader</tt>.
     * @see #openStream
     */
    public void appendData( Reader reader ) 
        throws IOException {

        BufferedReader bread = new BufferedReader( reader );

        char raw[] = new char[4096];
        int len = -1;

        while ( ( len = bread.read( raw ) ) != -1 ) {
            appendData( String.valueOf( raw, 0, len ) );
        }
    }

    /**
     * Close the document stream opened with {@link #openStream}.
     *
     * @see #openStream
     */
    public void closeStream() {
        gtk_moz_embed_close_stream( getHandle() );
    }

    /**
     * Get the link message for the current cursor position.
     *
     * @see org.gnu.gtkmozembed.event.BrowsingEvent.Type#LINK_MESSAGE
     */
    public String getLinkMessage() {
        return gtk_moz_embed_get_link_message( getHandle() );
    }

    /**
     * Get the current JavaScript status.
     *
     * @see org.gnu.gtkmozembed.event.BrowsingEvent.Type#JS_STATUS
     */
    public String getJsStatus() {
        return gtk_moz_embed_get_js_status( getHandle() );
    }

    /**
     * Get the current document title.
     *
     * @see org.gnu.gtkmozembed.event.BrowsingEvent.Type#TITLE
     */
    public String getTitle() {
        return gtk_moz_embed_get_title( getHandle() );
    }

    /**
     * Get the current document location.
     *
     * @see org.gnu.gtkmozembed.event.BrowsingEvent.Type#LOCATION
     */
    public String getLocation() {
        return gtk_moz_embed_get_location( getHandle() );
    }

    /*  Will add these later.
    public void setChromeMask() {
        gtk_moz_embed_set_chrome_mask( getHandle(), 1 );
    }

    public int getChromeMask() {
        return gtk_moz_embed_get_chrome_mask( getHandle() );
    }
    */

    /**
     * Add a listener for browsing events.
     *
     * @see org.gnu.gtkmozembed.event.BrowsingEvent
     */
    public void addBrowsingListener( BrowsingListener listener ) {
        int i = findListener( browsingListeners, listener );
        if ( i == -1 ) {
            if ( browsingListeners == null ) {
                evtMap.initialize( this, BrowsingEvent.Type.LINK_MESSAGE );
                evtMap.initialize( this, BrowsingEvent.Type.JS_STATUS );
                evtMap.initialize( this, BrowsingEvent.Type.LOCATION );
                evtMap.initialize( this, BrowsingEvent.Type.TITLE );
                browsingListeners = new Vector();
            }
            browsingListeners.addElement( listener );
        }
    }

    /**
     * Remove a browsing event listener.
     *
     * @see #addBrowsingListener( BrowsingListener )
     */
    public void removeBrowsingListener( BrowsingListener listener ) {
        int i = findListener( browsingListeners, listener );
        if ( i > -1 ) {
            browsingListeners.remove( i );
        }
        if ( browsingListeners.size() == 0 ) {
            evtMap.uninitialize( this, BrowsingEvent.Type.LINK_MESSAGE );
            evtMap.uninitialize( this, BrowsingEvent.Type.JS_STATUS );
            evtMap.uninitialize( this, BrowsingEvent.Type.LOCATION );
            evtMap.uninitialize( this, BrowsingEvent.Type.TITLE );
            browsingListeners = null;
        }
    }

    /**
     * Add a listener for net events.
     *
     * @see org.gnu.gtkmozembed.event.NetEvent
     */
    public void addNetListener( NetListener listener ) {
        int i = findListener( netListeners, listener );
        if ( i == -1 ) {
            if ( netListeners == null ) {
                evtMap.initialize( this, NetEvent.Type.PROGRESS );
                evtMap.initialize( this, NetEvent.Type.PROGRESS_ALL );
                evtMap.initialize( this, NetEvent.Type.NET_STATE );
                evtMap.initialize( this, NetEvent.Type.NET_STATE_ALL );
                evtMap.initialize( this, NetEvent.Type.NET_START );
                evtMap.initialize( this, NetEvent.Type.NET_STOP );
                netListeners = new Vector();
            }
            netListeners.addElement( listener );
        }
    }

    /**
     * Remove a net event listener.
     *
     * @see #addNetListener( NetListener )
     */
    public void removeNetListener( NetListener listener ) {
        int i = findListener( netListeners, listener );
        if ( i > -1 ) {
            netListeners.remove( i );
        }
        if ( netListeners.size() == 0 ) {
            evtMap.uninitialize( this, NetEvent.Type.PROGRESS );
            evtMap.uninitialize( this, NetEvent.Type.PROGRESS_ALL );
            evtMap.uninitialize( this, NetEvent.Type.NET_STATE );
            evtMap.uninitialize( this, NetEvent.Type.NET_STATE_ALL );
            evtMap.uninitialize( this, NetEvent.Type.NET_START );
            evtMap.uninitialize( this, NetEvent.Type.NET_STOP );
            netListeners = null;
        }
    }



    /**
     * Implementation method to build an EventMap for this widget class.
     * Not useful (or supported) for application use.
     */
    private static void addBrowsingEvents( EventMap anEvtMap ) {
        anEvtMap.addEvent( "link_message", "handleBrowsingEventLinkMsg", 
                           BrowsingEvent.Type.LINK_MESSAGE,
                           BrowsingListener.class );
        anEvtMap.addEvent( "js_status", "handleBrowsingEventJsStatus", 
                           BrowsingEvent.Type.JS_STATUS,
                           BrowsingListener.class );
        anEvtMap.addEvent( "location", "handleBrowsingEventLocation", 
                           BrowsingEvent.Type.LOCATION,
                           BrowsingListener.class );
        anEvtMap.addEvent( "title", "handleBrowsingEventTitle", 
                           BrowsingEvent.Type.TITLE,
                           BrowsingListener.class );
    }
    private void handleBrowsingEvent( BrowsingEvent.Type type ) {
        BrowsingEvent anEvent = new BrowsingEvent( this, type );
        fireBrowsingEvent( anEvent );
    }
    private void handleBrowsingEventLinkMsg() {
        handleBrowsingEvent( BrowsingEvent.Type.LINK_MESSAGE );
    }
    private void handleBrowsingEventJsStatus() {
        handleBrowsingEvent( BrowsingEvent.Type.JS_STATUS );
    }
    private void handleBrowsingEventLocation() {
        handleBrowsingEvent( BrowsingEvent.Type.LOCATION );
    }
    private void handleBrowsingEventTitle() {
        handleBrowsingEvent( BrowsingEvent.Type.TITLE );
    }
    private void fireBrowsingEvent( BrowsingEvent event ) {
        if ( browsingListeners == null ) {
            return;
        }
        int size = browsingListeners.size();
        int i = 0;
        while ( i < size ) {
            BrowsingListener ml = 
                (BrowsingListener)browsingListeners.elementAt( i );
            switch( event.getType().getID() ) {
            case 1:
                ml.linkMessageChanged( event );
                break;
            case 2:
                ml.jsStatusChanged( event );
                break;
            case 3:
                ml.locationChanged( event );
                break;
            case 4:
                ml.titleChanged( event );
                break;
            }
            i++;
        }
    }

    private static void addNetEvents( EventMap anEvtMap ) {
        anEvtMap.addEvent( "progress", "handleProgress", 
                           NetEvent.Type.PROGRESS,
                           NetListener.class );
        anEvtMap.addEvent( "progress_all", "handleProgressAll", 
                           NetEvent.Type.PROGRESS_ALL,
                           NetListener.class );
        anEvtMap.addEvent( "net_state", "handleNetState", 
                           NetEvent.Type.NET_STATE,
                           NetListener.class );
        anEvtMap.addEvent( "net_state_all", "handleNetStateAll", 
                           NetEvent.Type.NET_STATE_ALL,
                           NetListener.class );
        anEvtMap.addEvent( "net_start", "handleNetStart", 
                           NetEvent.Type.NET_START,
                           NetListener.class );
        anEvtMap.addEvent( "net_stop", "handleNetStop", 
                           NetEvent.Type.NET_STOP,
                           NetListener.class );
    }
    private void handleProgress( int cur, int max ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.PROGRESS );
        evt.setCurrent( cur );
        evt.setMaximum( max );
        fireNetEvent( evt );
    }
    private void handleProgressAll( Handle uri , int cur, int max ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.PROGRESS_ALL );
        evt.setURI(getStringFromHandle(uri));
        evt.setCurrent( cur );
        evt.setMaximum( max );
        fireNetEvent( evt );
    }
    private void handleProgressAll( String uri, int cur, int max ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.PROGRESS_ALL );
        evt.setURI( uri );
        evt.setCurrent( cur );
        evt.setMaximum( max );
        fireNetEvent( evt );
    }
    private void handleNetState( int state, int status ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.NET_STATE );
        evt.setState( state );
        evt.setStatus( status );
        fireNetEvent( evt );
    }
    private void handleNetStateAll( String uri, int state, int status ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.NET_STATE_ALL );
        evt.setURI( uri );
        evt.setState( state );
        evt.setStatus( status );
        fireNetEvent( evt );
    }
    private void handleNetStateAll( Handle uri, int state, int status ) {
        NetEvent evt = new NetEvent( this, NetEvent.Type.NET_STATE_ALL );
        evt.setURI(getStringFromHandle(uri));
        evt.setState( state );
        evt.setStatus( status );
        fireNetEvent( evt );
    }
    private void handleNetStart() {
        NetEvent evt = new NetEvent( this, NetEvent.Type.NET_START );
        fireNetEvent( evt );
    }
    private void handleNetStop() {
        NetEvent evt = new NetEvent( this, NetEvent.Type.NET_STOP );
        fireNetEvent( evt );
    }
    private void fireNetEvent( NetEvent event ) {
        if ( netListeners == null ) {
            return;
        }
        int size = netListeners.size();
        int i = 0;
        while ( i < size ) {
            NetListener ml = 
                (NetListener)netListeners.elementAt( i );
            switch( event.getType().getID() ) {
            case 1:
                ml.progressUpdate( event );
                break;
            case 2:
                ml.progressAllUpdate( event );
                break;
            case 3:
                ml.netStateChange( event );
                break;
            case 4:
                ml.netStateAllChange( event );
                break;
            case 5:
                ml.netStart( event );
                break;
            case 6:
                ml.netStop( event );
                break;
            }
            i++;
        }
    }

    /*
      later:

      Widget{Event,Listener}
      DomKey{Event,Listener}
      DomMouse{Event,Listener}
      State{Event,Listener} (maybe in Browser? or Net?)
    */
    
    //
    // Native methods that map to the C functions.
    //

    native static final private int gtk_moz_embed_get_type();
    native static final private Handle gtk_moz_embed_new();
    native static final private void gtk_moz_embed_push_startup();
    native static final private void gtk_moz_embed_pop_startup();
    native static final private void gtk_moz_embed_set_comp_path(String aPath);
    native static final private void gtk_moz_embed_set_profile_path(String aDir, String aName);
    native static final private void gtk_moz_embed_load_url(Handle embed, String url);
    native static final private void gtk_moz_embed_stop_load(Handle embed);
    native static final private boolean gtk_moz_embed_can_go_back(Handle embed);
    native static final private boolean gtk_moz_embed_can_go_forward(Handle embed);
    native static final private void gtk_moz_embed_go_back(Handle embed);
    native static final private void gtk_moz_embed_go_forward(Handle embed);
    native static final private void gtk_moz_embed_render_data(Handle embed, String data, int len, String base_uri, String mime_type);
    native static final private void gtk_moz_embed_open_stream(Handle embed, String base_uri, String mime_type);
    native static final private void gtk_moz_embed_append_data(Handle embed, String data, int len);
    native static final private void gtk_moz_embed_close_stream(Handle embed);
    native static final private String gtk_moz_embed_get_link_message(Handle embed);
    native static final private String gtk_moz_embed_get_js_status(Handle embed);
    native static final private String gtk_moz_embed_get_title(Handle embed);
    native static final private String gtk_moz_embed_get_location(Handle embed);
    native static final private void gtk_moz_embed_reload(Handle embed, int flags);
    native static final private void gtk_moz_embed_set_chrome_mask(Handle embed, int flags);
    native static final private int gtk_moz_embed_get_chrome_mask(Handle embed);
    // Not really useful.
    //native static final private Handle gtk_moz_embed_single_get();

    // These methods generate link errors and they are not really important.
    //native static final private int gtk_moz_embed_status_enums_get_type();
    //native static final private int gtk_moz_embed_reload_flags_get_type();
    //native static final private int gtk_moz_embed_chrome_flags_get_type();


    // ------------------------------------------------------------------------
    // static init code
    static {
        System.loadLibrary("gtkmozembedjni-" + Config.GTKMOZEMBED_API_VERSION);
        addBrowsingEvents(evtMap);
        addNetEvents(evtMap);
    }

}
