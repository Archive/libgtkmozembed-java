/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package org.gnu.gtkmozembed;

import java.util.Hashtable;
import org.gnu.glib.Flags;

/**
 * Flags defining the various document reloading schemes.
 */
public class ReloadFlags extends Flags {

    private static final int _NORMAL = 0;
    private static final int _BYPASSCACHE = 1;
    private static final int _BYPASSPROXY = 2;
    private static final int _BYPASSPROXYANDCACHE = 3;
    private static final int _CHARSETCHANGE = 4;

    /**
     *
     */
    public static final ReloadFlags NORMAL = 
        new ReloadFlags( _NORMAL );
    /**
     *
     */
    public static final ReloadFlags BYPASSCACHE = 
        new ReloadFlags( _BYPASSCACHE );
    /**
     *
     */
    public static final ReloadFlags BYPASSPROXY = 
        new ReloadFlags( _BYPASSPROXY );
    /**
     *
     */
    public static final ReloadFlags BYPASSPROXYANDCACHE = 
        new ReloadFlags( _BYPASSPROXYANDCACHE );
    /**
     *
     */
    public static final ReloadFlags CHARSETCHANGE = 
        new ReloadFlags( _CHARSETCHANGE );

    private static final ReloadFlags[] staticOnes = 
        new ReloadFlags[] {
            NORMAL, BYPASSCACHE, BYPASSPROXY, 
            BYPASSPROXYANDCACHE, CHARSETCHANGE
        };

    private static Hashtable dynamicExtras;
    private static final ReloadFlags special = 
        new ReloadFlags( 0 );

    public static ReloadFlags intern( int value ) {
        if ( value < staticOnes.length ) {
            return staticOnes[ value ];
        }
        special.value_ = value;
        if ( dynamicExtras == null ) {
            dynamicExtras = new Hashtable();
        }
        ReloadFlags already = 
            (ReloadFlags)dynamicExtras.get( special );
        if ( already == null ) {
            already = new ReloadFlags( value );
            dynamicExtras.put( already, already );
        }
        return already;
    }

    public ReloadFlags( int value ) {
        value_ = value;
    }
}
