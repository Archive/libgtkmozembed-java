/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

#include <jni.h>
#include <gtkmozembed.h>
#include <glib-java/jg_jnu.h>
#include <gtk_java.h>

#ifdef __cplusplus
extern "C" 
{
#endif

JNIEXPORT jint JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_moz_embed_get_type();
}

JNIEXPORT jobject JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1new
(JNIEnv *env, jclass cls)
{
	return getGObjectHandle(env, G_OBJECT(gtk_moz_embed_new()));
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1push_1startup
(JNIEnv *env, jclass cls)
{
    gtk_moz_embed_push_startup();
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1pop_1startup
(JNIEnv *env, jclass cls)
{
    gtk_moz_embed_pop_startup();
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1set_1comp_1path
(JNIEnv *env, jclass cls, jstring aPath)
{
    char * aPath_g = (char *)(*env)->GetStringUTFChars(env, aPath, 0);
    gtk_moz_embed_set_comp_path(aPath_g);
    (*env)->ReleaseStringUTFChars(env, aPath, aPath_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1set_1profile_1path
(JNIEnv *env, jclass cls, jstring aDir, jstring aName)
{
    char * aDir_g = (char *)(*env)->GetStringUTFChars(env, aDir, 0);
    char * aName_g = (char *)(*env)->GetStringUTFChars(env, aName, 0);
    gtk_moz_embed_set_profile_path(aDir_g, aName_g);
    (*env)->ReleaseStringUTFChars(env, aDir, aDir_g);
    (*env)->ReleaseStringUTFChars(env, aName, aName_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1load_1url
(JNIEnv *env, jclass cls, jobject embed, jstring url)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    const char * url_g = (const char *)(*env)->GetStringUTFChars(env, url, 0);
    gtk_moz_embed_load_url(embed_g, url_g);
    (*env)->ReleaseStringUTFChars(env, url, url_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1stop_1load
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    gtk_moz_embed_stop_load(embed_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1can_1go_1back
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (jboolean)gtk_moz_embed_can_go_back(embed_g);
}

JNIEXPORT jboolean JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1can_1go_1forward
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (jboolean)gtk_moz_embed_can_go_forward(embed_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1go_1back
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    gtk_moz_embed_go_back(embed_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1go_1forward
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    gtk_moz_embed_go_forward(embed_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1render_1data
(JNIEnv *env, jclass cls, jobject embed, jstring data, jint len, jstring base_uri, jstring mime_type)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    const char * data_g = (const char *)(*env)->GetStringUTFChars(env, data, 0);
    guint32 len_g = (guint32)len;
    const char * base_uri_g = (const char *)(*env)->GetStringUTFChars(env, base_uri, 0);
    const char * mime_type_g = (const char *)(*env)->GetStringUTFChars(env, mime_type, 0);
    gtk_moz_embed_render_data(embed_g, data_g, len_g, base_uri_g, mime_type_g);
    (*env)->ReleaseStringUTFChars(env, data, data_g);
    (*env)->ReleaseStringUTFChars(env, base_uri, base_uri_g);
    (*env)->ReleaseStringUTFChars(env, mime_type, mime_type_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1open_1stream
(JNIEnv *env, jclass cls, jobject embed, jstring base_uri, jstring mime_type)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    const char * base_uri_g = (const char *)(*env)->GetStringUTFChars(env, base_uri, 0);
    const char * mime_type_g = (const char *)(*env)->GetStringUTFChars(env, mime_type, 0);
    gtk_moz_embed_open_stream(embed_g, base_uri_g, mime_type_g);
    (*env)->ReleaseStringUTFChars(env, base_uri, base_uri_g);
    (*env)->ReleaseStringUTFChars(env, mime_type, mime_type_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1append_1data
(JNIEnv *env, jclass cls, jobject embed, jstring data, jint len)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    const char * data_g = (const char *)(*env)->GetStringUTFChars(env, data, 0);
    guint32 len_g = (guint32)len;
    gtk_moz_embed_append_data(embed_g, data_g, len_g);
    (*env)->ReleaseStringUTFChars(env, data, data_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1close_1stream
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    gtk_moz_embed_close_stream(embed_g);
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1link_1message
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (*env)->NewStringUTF(env, gtk_moz_embed_get_link_message(embed_g));
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1js_1status
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (*env)->NewStringUTF(env, gtk_moz_embed_get_js_status(embed_g));
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1title
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (*env)->NewStringUTF(env, gtk_moz_embed_get_title(embed_g));
}

JNIEXPORT jstring JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1location
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (*env)->NewStringUTF(env, gtk_moz_embed_get_location(embed_g));
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1reload
(JNIEnv *env, jclass cls, jobject embed, jint flags)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    gint32 flags_g = (gint32)flags;
    gtk_moz_embed_reload(embed_g, flags_g);
}

JNIEXPORT void JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1set_1chrome_1mask
(JNIEnv *env, jclass cls, jobject embed, jint flags)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    guint32 flags_g = (guint32)flags;
    gtk_moz_embed_set_chrome_mask(embed_g, flags_g);
}

JNIEXPORT jint JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1get_1chrome_1mask
(JNIEnv *env, jclass cls, jobject embed)
{
    GtkMozEmbed * embed_g = (GtkMozEmbed *)getPointerFromHandle(env, embed);
    return (jint)gtk_moz_embed_get_chrome_mask(embed_g);
}

// Not useful or buggy.
/*
JNIEXPORT jobject JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1single_1get
(JNIEnv *env, jclass cls)
{
    return getHandleFromPointer(env, gtk_moz_embed_single_get());
}

JNIEXPORT jint JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1status_1enums_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_moz_embed_status_enums_get_type();
}

JNIEXPORT jint JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1reload_1flags_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_moz_embed_reload_flags_get_type();
}

JNIEXPORT jint JNICALL Java_org_gnu_gtkmozembed_MozEmbed_gtk_1moz_1embed_1chrome_1flags_1get_1type
(JNIEnv *env, jclass cls)
{
    return (jint)gtk_moz_embed_chrome_flags_get_type();
}
*/

#ifdef __cplusplus
}
#endif
