/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package TestMoz;

import org.gnu.gtk.Widget;
import org.gnu.gtk.ReliefStyle;
import org.gnu.gtk.Image;
import org.gnu.gtk.IconSize;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.Button;
import org.gnu.gtk.Label;
import org.gnu.gtk.HBox;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.ButtonEvent;

public class ClosableNotebookTabLabel extends HBox {

    protected Widget body = null;
    protected Notebook book = null;
    protected boolean simplifytabs = true;

    public ClosableNotebookTabLabel( String labeltext, 
                                     Widget body, Notebook book ) {
        super( false, 5 );

        this.body = body;
        this.book = book;

        Label lbl = new Label( labeltext );
        Button btn = new Button();
        btn.add( new Image( GtkStockItem.CLOSE, IconSize.MENU ) );
        btn.setRelief( ReliefStyle.NONE );
        btn.addListener( new CloseButtonListener() );

        packStart( lbl );
        packStart( btn );
        showAll();
    }

    public void setSimplifyTabs( boolean val ) { simplifytabs = val; }
    public boolean getSimplifyTabs() { return simplifytabs; }

    protected class CloseButtonListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                book.removePage( book.pageNum( body ) );
                if ( simplifytabs ) {
                    if ( book.getNumPages() < 2 ) {
                        book.setShowTabs( false );
                    }
                }
            }
        }
    }
}
