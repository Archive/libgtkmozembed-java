/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package TestMoz;

import org.gnu.gdk.ModifierType;
import org.gnu.gtk.Button;
import org.gnu.gtk.Entry;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.GtkStockItem;
import org.gnu.gtk.HBox;
import org.gnu.gtk.HSeparator;
import org.gnu.gtk.IconSize;
import org.gnu.gtk.Image;
import org.gnu.gtk.Notebook;
import org.gnu.gtk.ProgressBar;
import org.gnu.gtk.ReliefStyle;
import org.gnu.gtk.StatusBar;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.EntryEvent;
import org.gnu.gtk.event.EntryListener;
import org.gnu.gtk.event.KeyEvent;
import org.gnu.gtk.event.KeyListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;
import org.gnu.gtk.event.NotebookEvent;
import org.gnu.gtk.event.NotebookListener;

import org.gnu.gtkmozembed.MozEmbed;
import org.gnu.gtkmozembed.ReloadFlags;
import org.gnu.gtkmozembed.event.BrowsingEvent;
import org.gnu.gtkmozembed.event.BrowsingAdapter;
import org.gnu.gtkmozembed.event.NetEvent;
import org.gnu.gtkmozembed.event.NetAdapter;

/**
 * Test class for <tt>MozEmbed</tt>.  Implements a very simple browser
 * interface.  
 * <p> 
 * <ul>
 * <li>Typing Ctrl-T will open a new tab (although this is pretty
 * buggy at the moment).</li>
 * <li>Typing Ctrl-Q exits the browser.</li>
 * </ul>
 */
public class TestMoz {

    private static final String DEFAULT_PAGE = "http://www.google.com/";

    private Window window;
    private Entry ent;
    private StatusBar status;
    private ProgressBar progress;
    private Notebook notebook;
    private Button back;
    private Button forward;
    private Button reload;
    private Button stop;

    public TestMoz() {

        window = new Window( WindowType.TOPLEVEL );
	window.setTitle( "Test MozEmbed" );
	window.addListener( new Life() );
	window.addListener( new Keys() );

        VBox vbox = new VBox( false, 0 );

        HBox hbox = new HBox( false, 0 );
        hbox.setBorderWidth( 5 );

        back = new Button( "" );
        back.setImage( new Image( GtkStockItem.GO_BACK, IconSize.BUTTON ) );
        back.setRelief( ReliefStyle.NONE );
        back.setSensitive( false );
        back.addListener( new BackListener() );
        hbox.packStart( back, false, false, 5 );
        forward = new Button( "" );
        forward.setImage( new Image( GtkStockItem.GO_FORWARD, 
                                     IconSize.BUTTON ) );
        forward.setRelief( ReliefStyle.NONE );
        forward.setSensitive( false );
        forward.addListener( new ForwardListener() );
        hbox.packStart( forward, false, false, 5 );
        reload = new Button( "" );
        reload.setImage( new Image( GtkStockItem.REFRESH, IconSize.BUTTON ) );
        reload.setRelief( ReliefStyle.NONE );
        reload.setSensitive( false );
        reload.addListener( new ReloadListener() );
        hbox.packStart( reload, false, false, 5 );
        stop = new Button( "" );
        stop.setImage( new Image( GtkStockItem.STOP, IconSize.BUTTON ) );
        stop.setRelief( ReliefStyle.NONE );
        stop.setSensitive( false );
        stop.addListener( new StopListener() );
        hbox.packStart( stop, false, false, 5 );
        Image img = new Image( GtkStockItem.JUMP_TO, IconSize.BUTTON );
        hbox.packStart( img, false, false, 5 );
        ent = new Entry();
        hbox.packStart( ent );
        ent.addListener( new LocationListener() );
        ent.setText( "http://www.kernel.org" );
        Button gobtn = new Button( "Go" );
        gobtn.setRelief( ReliefStyle.NONE );
        gobtn.addListener( new GoListener() );
        hbox.packStart( gobtn, false, false, 0 );

        vbox.packStart( hbox, false, false, 0 );

        HSeparator sep = new HSeparator();
        vbox.packStart( sep, false, false, 0 );

        notebook = new Notebook();
        notebook.addListener( new Tabber() );
        notebook.setShowTabs( false );
        //notebook.setShowBorder( false );
        //notebook.setBorderWidth( 0 );
        MozEmbed mozEmbed = getNewMoz();
        ClosableNotebookTabLabel tab = 
            new ClosableNotebookTabLabel( "tab", mozEmbed, notebook );
        notebook.appendPage( mozEmbed, tab );
        vbox.packStart( notebook );

        loadURL( DEFAULT_PAGE );

        HBox sbox = new HBox( false, 0 );
        progress = new ProgressBar();
        status = new StatusBar();
        sbox.packStart( progress, false, false, 0 );
        sbox.packStart( status );
        vbox.packStart( sbox, false, false, 0 );

	window.add( vbox );
        window.resize( 300, 200 );
        window.showAll();

    }

    public static void main( String[] args ) {
        Gtk.init( args );
        new TestMoz();
        Gtk.main();
    }

    private MozEmbed getNewMoz() {
        MozEmbed mozEmbed = new MozEmbed();
        mozEmbed.addBrowsingListener( new Browse() );
        mozEmbed.addNetListener( new Net() );
        return mozEmbed;
    }

    private void loadURL( String url ) {
        getCurrMozEmbed().loadURL( url );
    }

    private void setTitle() {
        MozEmbed moz = getCurrMozEmbed();
        String title = moz.getTitle();
        if ( title == null || title.equals( "" ) ) {
            title = "(Untitled)";
        }
        window.setTitle( title );
        notebook.setTabLabel( moz, 
                              new ClosableNotebookTabLabel( title, 
                                                            moz, notebook ) );
        notebook.showAll();
    }

    private void newTab() {
        MozEmbed moz = getNewMoz();
        notebook.appendPage( moz, 
                             new ClosableNotebookTabLabel( "(Untitled)", 
                                                           moz, notebook ) );
        notebook.setShowTabs( true );
        notebook.showAll();
    }

    private void setLocation() {
        MozEmbed moz = getCurrMozEmbed();
        ent.setText( moz.getLocation() );
    }

    protected MozEmbed getCurrMozEmbed() {
        return (MozEmbed)notebook.getPage( notebook.getCurrentPage() );
    }
        
    protected class Browse extends BrowsingAdapter {
        public void linkMessageChanged( BrowsingEvent event ) {
            String txt = getCurrMozEmbed().getLinkMessage();
            int id = status.getContextID( txt );
            status.push( id, txt );
            debug( event.getType().getName(), txt );
        }
        public void jsStatusChanged( BrowsingEvent event ) {
            String txt = getCurrMozEmbed().getJsStatus();
            int id = status.getContextID( txt );
            status.push( id, txt );
            debug( event.getType().getName(), txt );
        }
        public void locationChanged( BrowsingEvent event ) {
            setLocation();
            debug( event.getType().getName(), 
                   getCurrMozEmbed().getLocation() );
        }
        public void titleChanged( BrowsingEvent event ) {
            setTitle();
            debug( event.getType().getName(), getCurrMozEmbed().getTitle() );
        }
        protected void debug( String type, String txt ) {
            StringBuffer buf = new StringBuffer();
            buf.append( type ).append( ": " );
            buf.append( txt );
            System.out.println( buf.toString() );
        }
    }

    protected class Net extends NetAdapter {
        public void progressUpdate( NetEvent event ) {
            progress.pulse();
            debug( event );
        }
        public void netStateChange( NetEvent event ) {
            progress.pulse();
            debug( event );
        }
        public void netStart( NetEvent event ) {
            stop.setSensitive( true );
            reload.setSensitive( false );
            progress.pulse();
            debug( event );
        }
        public void netStop( NetEvent event ) {
            progress.setFraction( 0.0 );
            stop.setSensitive( false );
            reload.setSensitive( true );
            MozEmbed moz = getCurrMozEmbed();
            if ( !moz.canGoBack() ) {
                back.setSensitive( false );
            } else {
                back.setSensitive( true );
            }
            if ( !moz.canGoForward() ) {
                forward.setSensitive( false );
            } else {
                forward.setSensitive( true );
            }
            debug( event );
        }
        protected void debug( NetEvent event ) {
            StringBuffer buf = new StringBuffer();
            buf.append( event.getType().getName() ).append( ": " );
            buf.append( event.getCurrent() ).append( "," );
            buf.append( event.getMaximum() ).append( "," );
            buf.append( event.getURI() );
            System.out.println( buf.toString() );
        } 
    }

    //
    // this whole tabbing thing doesn't work right because the notif
    // is before the actual switch to the new tab so the
    // getCurrentPage returns the number of the old page.
    //
    // Also there seems to be some display updating issues with multiple
    // tabs.  will check this out soon.
    //
    protected class Tabber implements NotebookListener {
        public void notebookEvent( NotebookEvent event ) {
            System.out.println( "notebookEvent: " + 
                                notebook.getCurrentPage() );
            if ( notebook.getCurrentPage() >= 0 ) {
                setTitle();
                setLocation();
            }
        }
    }

    /**
     * Listener for events on the URL entry widget.
     */
    protected class LocationListener implements EntryListener {
        public void entryEvent( EntryEvent event ) {
            if ( event.isOfType( EntryEvent.Type.ACTIVATE ) ) {
                String loc = ent.getText();
                System.out.println( "activated! - " + loc );
                loadURL( loc );
            }
        }
    }

    /**
     * Listener for events on the GO button.
     */
    protected class GoListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                String loc = ent.getText();
                System.out.println( "activated! - " + loc );
                loadURL( loc );
            }
        }
    }
    /**
     * Listener for events on the BACK button.
     */
    protected class BackListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                MozEmbed moz = getCurrMozEmbed();
                if ( moz.canGoBack() ) {
                    moz.goBack();
                }
            }
        }
    }
    /**
     * Listener for events on the FORWARD button.
     */
    protected class ForwardListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                MozEmbed moz = getCurrMozEmbed();
                if ( moz.canGoForward() ) {
                    moz.goForward();
                }
            }
        }
    }
    /**
     * Listener for events on the RELOAD button.
     */
    protected class ReloadListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                getCurrMozEmbed().reload( ReloadFlags.NORMAL );
            }
        }
    }
    /**
     * Listener for events on the STOP button.
     */
    protected class StopListener implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {
                getCurrMozEmbed().stopLoad();
            }
        }
    }
    
    /**
     * Listener for key events on the main window.  Used to catch the Ctrl-C
     * and Ctrl-T.
     */
    protected class Keys implements KeyListener {
        public boolean keyEvent( KeyEvent event ) {
            // Ctrl-Q quits!
            if ( event.getKeyval() == 113 && 
                 event.getModifierKey() == ModifierType.CONTROL_MASK &&
                 event.isOfType( KeyEvent.Type.KEY_RELEASED ) ) {
                Gtk.mainQuit();
                return false;
            }
            // Ctrl-T opens new tab.
            if ( event.getKeyval() == 116 &&
                 event.getModifierKey() == ModifierType.CONTROL_MASK &&
                 event.isOfType( KeyEvent.Type.KEY_RELEASED ) ) {
                System.out.println( "keyEvent!" );
                newTab();
                return false;
            }
            return false;
        }
    }

    /**
     *  Listener for life cycle events on the main window.
     */
    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent( LifeCycleEvent event ) {}
        public boolean lifeCycleQuery( LifeCycleEvent event ) {
            if ( event.isOfType( LifeCycleEvent.Type.DESTROY ) || 
                 event.isOfType( LifeCycleEvent.Type.DELETE ) ) {
                Gtk.mainQuit();
            }
            return false;
        }
    }

}
