/*
 * Java-Gnome Bindings Library
 *
 * Copyright 2005 the Java-Gnome Team, all rights reserved.
 *
 * The Java-Gnome bindings library is free software distributed under
 * the terms of the GNU Library General Public License version 2.
 */

package SelfLoadingMoz;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
//import java.io.FileReader;
import java.io.IOException;

import org.gnu.gdk.ModifierType;
import org.gnu.gtk.Button;
import org.gnu.gtk.Gtk;
import org.gnu.gtk.VBox;
import org.gnu.gtk.Window;
import org.gnu.gtk.WindowType;
import org.gnu.gtk.event.ButtonEvent;
import org.gnu.gtk.event.ButtonListener;
import org.gnu.gtk.event.KeyEvent;
import org.gnu.gtk.event.KeyListener;
import org.gnu.gtk.event.LifeCycleEvent;
import org.gnu.gtk.event.LifeCycleListener;

import org.gnu.gtkmozembed.MozEmbed;

/**
 * Test class for loading documents using the <tt>openStream</tt>,
 * <tt>appendData</tt>, <tt>closeStream</tt> methods of MozEmbed.
 */
public class SelfLoadingMoz {

    private static final String DEFAULT_PAGE = "SelfLoadingMoz/test.html";

    private Window window;
    private MozEmbed mozEmbed;

    public SelfLoadingMoz() {
        window = new Window( WindowType.TOPLEVEL );
	window.setTitle( "SelfLoadingMoz Test" );
	window.addListener( new Life() );
	window.addListener( new Keys() );

        VBox vbox = new VBox( false, 0 );
        mozEmbed = new MozEmbed();
        vbox.packStart( mozEmbed );
        Button load = new Button( "Load" );
        load.addListener( new Loader() );
        vbox.packStart( load, false, false, 0 );

	window.add( vbox );
        window.resize( 300, 200 );
        window.showAll();
    }

    public static void main( String[] args ) {
        Gtk.init( args );
        new SelfLoadingMoz();
        Gtk.main();
    }

    /**
     * Listener for events on the load button.
     */
    protected class Loader implements ButtonListener {
        public void buttonEvent( ButtonEvent event ) {
            if ( event.isOfType( ButtonEvent.Type.CLICK ) ) {

                // Load the document from the file DEFAULT_PAGE.
                FileInputStream fstream = null;
                try {
                    File f = new File( DEFAULT_PAGE );
                    fstream = new FileInputStream( f );
                    //
                    // Load the data from the stream.
                    //
                    mozEmbed.renderData( fstream, 
                                         "file://" + f.getAbsolutePath(),
                                         "text/html" );

                    //
                    // You can also load from a Reader or a String.
                    //

                } catch ( FileNotFoundException fe ) {
                    System.out.println( "can't load: " + DEFAULT_PAGE );
                    System.out.println( fe );
                    return;
                } catch ( IOException ex ) {
                    System.out.println( ex );
                }

                //
                // You could do this the hard way too...
                //
                /*
                FileReader freader = null;
                try { 
                    freader = new FileReader( DEFAULT_PAGE );
                } catch( FileNotFoundException fe ) {
                    System.out.println( "can't load: " + DEFAULT_PAGE );
                    System.out.println( fe );
                    return;
                }
                StringBuffer buf = new StringBuffer();
                char raw[] = new char[4096];
                int len = -1;
                try {
                    while( (len = freader.read( raw )) != -1 ) {
                        buf.append( raw, 0, len );
                    }
                } catch( IOException ie ) {
                    System.out.println( "IO excpt: " + ie );
                }
                System.out.println( "loading: " + buf.toString() );
                
                // Open the stream.
                mozEmbed.openStream( "file:/" + DEFAULT_PAGE, "text/html" );
                // Append data to the stream.  You can also call this method
                // with an InputStream or a Reader.  You can load multiple
                // data's by calling this multiple times.
                mozEmbed.appendData( buf.toString() );
                // Close the stream.
                mozEmbed.closeStream();
                */

                System.out.println( "loaded." );
            }
        }
    }

    /**
     * Listener for key events on the main window.  Used to catch the Ctrl-C.
     */
    protected class Keys implements KeyListener {
        public boolean keyEvent( KeyEvent event ) {
            // Ctrl-Q quits!
            if ( event.getKeyval() == 113 && 
                 event.getModifierKey() == ModifierType.CONTROL_MASK &&
                 event.isOfType( KeyEvent.Type.KEY_RELEASED ) ) {
                Gtk.mainQuit();
                return false;
            }
            // Ctrl-T opens new tab.
            /*
            if ( event.getKeyval() == 116 &&
                 event.getModifierKey() == ModifierType.CONTROL_MASK &&
                 event.isOfType( KeyEvent.Type.KEY_RELEASED ) ) {
                System.out.println( "keyEvent!" );
                newTab();
                return false;
            }
            */
            return false;
        }
    }

    /**
     *  Listener for life cycle events on the main window.
     */
    protected class Life implements LifeCycleListener {
        public void lifeCycleEvent( LifeCycleEvent event ) {}
        public boolean lifeCycleQuery( LifeCycleEvent event ) {
            if ( event.isOfType( LifeCycleEvent.Type.DESTROY ) || 
                 event.isOfType( LifeCycleEvent.Type.DELETE ) ) {
                Gtk.mainQuit();
            }
            return false;
        }
    }

}
